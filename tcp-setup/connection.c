#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include "connection.h"
#define BUFFER_MAX 4

#define SA struct sockaddr


// startConnection: setup bind(), listen and servAddr
void startConnection(int sd,  struct sockaddr_in servAddr){
    // Binding newly created socket to given IP and verification
    if ((bind(sd, (SA*)&servAddr, sizeof(servAddr))) != 0) {
        printf("\nSocket non collegato correttamente...\n");
        exit(0);
    }
    else
        printf("\nSocket collegato correttamente..\n");
  
    // Now server is ready to listen and verification
    if ((listen(sd, 5)) != 0) {
        printf("\nAscolto del Server fallito...\n");
        exit(0);
    }
    else
        printf("\nServer in ascolto..\n");

}


// acceptConnection: allows client to connect and return the socket client
int acceptConnection(int sd, struct sockaddr_in clientAddr){
    
    socklen_t len = sizeof(clientAddr);

    int connfd = accept(sd, (SA*)&clientAddr, &len);
    assert(connfd >= 0);
    printf("Server ha accettato il client...\n");

    return connfd;
}



char *receiveMessage(int socketID){
    

    char slen [BUFFER_MAX + 1];
    memset(slen, '\0', BUFFER_MAX + 1);
    
    int byteRecv = 0;
    
    // ==> recv size of the incoming string
    if((byteRecv = (int) recv(socketID, slen, sizeof(slen), 0) <= 0)) {
        return NULL;
    }
    
    // =====> Convertion string to int of the msg size
    int len = atoi(slen);
    
    char* msg = calloc(len + 1, sizeof(char));
    
    // ==> recv the string
    if((byteRecv = (int) recv(socketID, msg, len, 0)) <= 0){
        return NULL;
    }
    
    return msg;
}


// Send Message
int sendMessage(int socketID, const char* message){
    
    int len = (int) strlen(message);
    
    char slen[BUFFER_MAX + 1];
    memset(slen, '\0', BUFFER_MAX + 1);
    snprintf(slen, sizeof(slen), "%d", len);
    
    
    int byteSend = 0;
    
    if( (byteSend = (int) send(socketID, slen, sizeof(slen), 0) <= 0))
        return 0;
    
    
    if((byteSend = (int) send(socketID, (char *) message, len, 0) <= 0 ))
        return 0;
    
    return 1;
}

