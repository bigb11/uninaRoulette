
#ifndef player_h
#define player_h
#include "../list/bids.h"
#include "../list/list.h"

struct TPlayer {
    char *name;
    char *surname;
    char *username;
    char *email;
    char *password;
    int coins;
    int idSocket;
    int socketSync;
    Bid listBids;
    
};

typedef struct TPlayer *Player;


struct TPlayerList {
    Player *listPlayers;
    int indexSize;
    int maxSize;
};

typedef struct TPlayerList *PlayerList;



struct TList {
    Player elem;
    char *date;
    struct TList *next;
};

typedef struct TList *List;


struct TBidList {
    Bid elem;
    struct TBidList *next;
};

typedef struct TBidList *BidList;




/*list Bid*/
BidList pushListBid(BidList top,BidList newBid);
BidList initNodeBid(Bid elem,BidList top);
void printBid(BidList top);



void printList(List top);
List pushList(List top, List newList);
List initNode(Player elem, List top);
int lenList(List top);
List deleteListPlayer(List logPlayer, int idSocket);
List insertHead(List top, Player key);
void dealloca(List top);
int checkPlayer(List top, char *username);



//Single Entity Management
Player initPlayer(char* name, char* surname, char* username, char* email, char* password, int coins);
Player convertPlayerFromString(char *dati);
void freePlayer(Player *p);



//List Players Management
PlayerList initPlayerList(int num_max);
void addPlayerToList(PlayerList list, Player newP);
void printPlayerList(PlayerList list);
void freeArrayList(PlayerList p);
Player checkUsername(List player, char *username);
//List deleteList(List startPlayer);

#endif
