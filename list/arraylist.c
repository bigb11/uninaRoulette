//
//  arraylist.c
//  rouletteGame
//
//  Created by Domenico Maione on 18/06/21.
//

#include "arraylist.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>





ArrList initialiseWithCapacity(int initialCapacity) {
    ArrList a = malloc(sizeof *a);
    a->capacity = initialCapacity;
    a->arr = malloc(a->capacity * sizeof *(a->arr));
    a->name = calloc(50, sizeof(struct AList));
    a->surname = calloc(50, sizeof(struct AList));
    a->username = calloc(50, sizeof(struct AList));
    a->email = calloc(50, sizeof(struct AList));
    a->password = calloc(50, sizeof(struct AList));
    a->coins = calloc(50, sizeof(struct AList));
    a->size = 0;
    return a;
}

ArrList initialise() {
    return initialiseWithCapacity(10);
}

bool append(ArrList a, int n)
{
    
    a->arr[a->size++] = n;
    return true;
}







bool insert(ArrList a, int index, int n) {
    if(index < 0)
        index = a->size + index;
    
    if(index > a->size || index < 0)
        return false;
    
    if(index == a->size) {
        append(a, n);
        return true;
    }
    
    a->arr[index] = n;
    a->size++;
    return true;
}
