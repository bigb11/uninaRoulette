#ifndef utilsstring_h
#define utilsstring_h

struct TNodeString {
    char* data;
    struct TNodeString *next;
};

typedef struct TNodeString* NodeString;

struct TListString {
    NodeString top;
};

typedef struct TListString* ListString;



struct SData
{
    char *name;
    char *surname;
    char *username;
    char *email;
    char *password;
    char *coins;
    struct SData* next;
};

typedef struct SData* TData;







NodeString initNodeString(char* str);
ListString initListString();
void addNodeString(ListString *list, NodeString new);
int dimDati (char *username, char *name, char *surname, char *email, char *password, char *coins);
int dimDatiLogin (char *username, char *password);
void initStaticString(char *str, int size);
char* concatString(const char* str1, const char* str2);
char *substring(char* str, int start, int end);
//bool containsSubString(const char* str, const char* sub);
char* intToString(int x);
ListString tokenize(char* str, char delimiterR, char delimiterL);
char* dateToString();


#endif
