#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <pthread.h>
#include "../tcp-setup/connection.h"
#include "../tcp-setup/config.h"
#include "../player/player.h"
#include "login-register/function-file.h"
#include "data/database.h"
#include "../string/utilsstring.h"
#include "../list/list.h"
#include "../list/bids.h"
#include "utils.h"

//9 LUGLIO 2021 15.26
#define MAX 80
#define PORT 8080
#define NMAX_PLAYER 126
#define SA struct sockaddr
#define STDIN 0


List listPlayer = NULL;
List startPlayer = NULL;
List currTop = NULL;
BidList gamer = NULL;

//MUTEX PRINCIPALE
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;

// VARIABILI DI CONDIZIONE PER IL PTHREAD BROADCAST NELLA FASE DI GIOCO
pthread_cond_t condWaitPlayers = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutexWaitPlayers = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_endGame = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_endGame = PTHREAD_COND_INITIALIZER;
//----------------------------------------------------------

//DICHIARAZIONE STATICA PER I THREADS BETTERCOMMUNICATION
pthread_t tidBets[100];

//VIARIABILE BOOLEANA PER L'ATTESA DI NUOVI PLAYERS CHE ENTRANO NELLA STANZA DI GIOCO
bool isEmptyList = true;
//-----------------------------------------------------------------------------------

//DICHIARAZIONI VARIABILI PER LA FASE DI GIOCO
int count = 20;
int indexT = 0;
int res = -1;
int idGame = 0;
char *status = "APERTO";
//--------------------------------------------


//FUNZIONE CHE ESTRAE UN NUMERO DA 0 A 36 IN MODO RANDOMICO
int RouletteGame()
{
    int n;
    time_t t;
    srand((unsigned) time(&t));
    n = rand() % 37;
    
    return n;
}
//---------------------------------------------------------



//THREAD SEMPRE ATTIVO CHE AGGIORNA IN UNA SOCKET SECONDARIA LO STATO DI GIOCO (APERTO/CHIUSO)
void *threadSync(void *arg)
{
    Player p = (Player)arg;
    
    while (true)
    {
        
        if((receiveMessage(p->socketSync)) == NULL)
        {
            close(p->idSocket);
            pthread_exit(NULL);
        }
        
        if(sendMessage(p->socketSync,status) == 0)
        {
            close(p->idSocket);
            pthread_exit(NULL);
        }
 
    }
    
    
    pthread_exit(NULL);
}
//-------------------------------------------------------------------------------------------




//THREAD PRINCIPALE CHE VIENE CREATO OGNI VOLTA CHE VIENE ACCETTATA LA CONNESSIONE AL SERVER
void *func(void *arg)
{
    
    //DICHIARAZIONI VARIABILI PER IL MENU PRINCIPALE
    char *username, *password,*email;
    int esito;
    int esito_check = 0;
    int scelta = 0;
    
    Player myPlayer = NULL; //STRUTTURA DEL PLAYER
    int clientid = *(int *)arg;
    
    char *stringScelta; //variabile che riceverà il valore della scelta del menu principale
    char *dati;
    char *datiLogin;
    char *dati_login = malloc(sizeof(char)*100);
    char rules[]= " Ogni utente, appena entrato in gioco riceverà un numero fisso di gettoni (100 coins) che potrà spendere puntandoli su uno dei numeri durante le sessioni di gioco.\nOgni sessione di gioco prevede una fase di raccolta delle puntate (15 secondi) seguita da una fase di gioco (giro della roulette) durante la quale cui si determina il numero vincente, si pagano i vincitori e si raccolgono i gettoni dai perdenti.\nOgni utente, una volta connesso al server, potrà accedere al tavolo da gioco e partecipare puntando su un numero.\nIl risultato di una sessione verrà notificato a tutti gli utenti che hanno puntato riportando anche i gettoni vinti o persi insieme al credito residuo in gettoni.\nGli utenti che avranno finito i gettoni saranno esclusi dal gioco";
    
    
    char *recv; //ricevo la lettera P con la socket creata solo per registrazione e login
   //------------------------------------------------------------------------------------------------------------------------------------
   
    
    //SYNC STEP 1 || 0 STEP
    if((recv = receiveMessage(clientid)) == NULL)
    {
        close(clientid);
        pthread_exit(NULL);
    }
    
    //SE RICEVO LA LETTERA P, VUOLE DIRE CHE IL CLIENT SI E' CONNESSO ALLA PRIMA SOCKET ASSEGNATA
    //SE RICEVE LA STRINGA SYNC, VUOLE DIRE CHE IL CLIENT SI E' CONNESSO SULLA SECONDA SOCKET (STATO DI GIOCO)
    if(strcmp(recv,"P")==0)
    {
        do
        {
            
            //1 STEP
            if((stringScelta = receiveMessage(clientid)) == NULL) //riceve la scelta del menu dal client
            {
                storyPlayers("si è disconnesso",NULL,clientid);
                close(clientid);
                pthread_exit(NULL);
            }
            
            scelta = atoi(stringScelta); //trasformo la scelta ricevuta dal client trasmorfandola in intero
            switch (scelta)
            {
                case 0:
                    //printf("\nUn visitatore %d ha chiesto di uscire...\n",clientid);
                    storyPlayers("ha chiesto di uscire",NULL,clientid);
                    
                    break;
                    
                case 1:
                    //REGISTRAZIONE
                    
                    //2 STEP
                    if((dati = receiveMessage(clientid)) == NULL) //riceve la scelta del menu dal client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    //FASE DOVE SI CONTROLLA SE L'UTENTE E' GIA' REGISTRATO
                    myPlayer = convertPlayerFromString(dati);
                    username = stringWithFormat(myPlayer->username,"U:[", "]");
                    email = stringWithFormat(myPlayer->email,"E:[", "]");
                    esito = check(username,email,scelta,dati_login);
                    //se esito è 0 allora la registrazione è avvenuta con successo
                    FILE *fp;
                    
                    if(esito !=0)
                        freePlayer(&myPlayer);
                    else
                    {
                        //scrivo nel file i dati del player poichè l'esito è 0
                        fp = fopen("./database/players.txt", "a");
                        fprintf(fp,"U:[%s] N:[%s] S:[%s] E:[%s] P:[%s] C:[%d]\n", myPlayer->username,myPlayer->name,myPlayer->surname,myPlayer->email,myPlayer->password,myPlayer->coins);
                        fclose(fp);
                        storyPlayers("si è registrato",myPlayer,clientid);
                        freePlayer(&myPlayer);
                    }
                    
                    //3 STEP
                    if((sendMessage(clientid,intToString(esito))) == 0) //invio l'esito della registrazione al client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    //4 STEP
                    if((sendMessage(clientid,dati)) == 0) //invio i dati della registrazione al client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    
                    break;
                    
                case 2:
                    //LOGIN
                    //5 STEP
                    if((datiLogin = receiveMessage(clientid)) == NULL)
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    myPlayer = convertPlayerFromString(datiLogin);
                    username = stringWithFormat(myPlayer->username,"U:[", "]");
                    password = stringWithFormat(myPlayer->password,"P:[", "]");
                    esito = check(username,password,scelta,dati_login);
                    esito_check = checkPlayer(listPlayer,myPlayer->username);
                    // printf("\nesito check %d\n",esito_check);
                    freePlayer(&myPlayer); //cancello la struttura player che mi serviva solo per il login
                    
                    
                    if(esito == 1 || esito_check == 1)
                        strcpy(dati_login,"ERRATO");
                    else
                    {
                        //SE L'UTENTE ESISTE E NON E' ANCORA LOGGATO, VIENE CREATA LA STRUTTURA PLAYER
                        myPlayer = convertPlayerFromString(dati_login); //ho creato la struttura completa del player
                        myPlayer->idSocket = clientid;
                        pthread_mutex_lock(&mutex);
                        listPlayer = insertHead(listPlayer,myPlayer);
                        logPlayersTxt(listPlayer); //salva l'utente loggato in un file txt
                        pthread_mutex_unlock(&mutex);
                        storyPlayers("ha loggato",myPlayer,clientid);
                    }
                    
                    
                    
                    //6 STEP
                    if((sendMessage(clientid,intToString(esito))) == 0) //invio l'esito del login al client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    
                    
                    //7 STEP
                    if((sendMessage(clientid,dati_login)) == 0) //invio l'esito del login al client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    if((sendMessage(clientid,intToString(esito_check))) == 0) //invio l'esito del login al client
                    {
                        storyPlayers("si è disconnesso",NULL,clientid);
                        close(clientid);
                        pthread_exit(NULL);
                    }
                    
                    
                    
                    //------FASE DI MENULOG----------
                    char *sceltaMenuLog;
                    int sceltaMenuLogInt;
                    char *lastNumber;
                    
                    
                    if(myPlayer != NULL && esito == 0)
                    {
                        do
                        {
                            //8 STEP
                            if((sceltaMenuLog = receiveMessage(myPlayer->idSocket)) == NULL) //ricevo lo start dal client
                            {
                                storyPlayers("si è disconnesso",myPlayer,clientid);
                                listPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                close(myPlayer->idSocket);
                                pthread_exit(NULL);
                            }
                            
                            sceltaMenuLogInt = atoi(sceltaMenuLog);
                            
                            switch (sceltaMenuLogInt)
                            {
                                case 0:
                                    
                                    storyPlayers("è uscito dal menu game",myPlayer,clientid);
                                    listPlayer = deleteListPlayer(listPlayer,clientid);
                                    break;
                                    
                                case 1:
                                    //EFFETTUA LA GIOCATA
                                    if(strcmp(status,"APERTO") == 0){
                                        
                                        isEmptyList = false;//il thread startGame non rimane più in attesa e inizia la fase delle puntate
                                        //è bloccato dal primo che ha chiamato questo segnale
                                        pthread_cond_signal(&condition);//lancio quindi un segnale di risveglio al thread startGame
                                        pthread_mutex_lock(&mutex); //blocco del mutex
                                        startPlayer = insertHead(startPlayer,myPlayer); //QUESTA LISTA CONTIENE I PLAYERS CHE GIOCANO
                                        pthread_mutex_unlock(&mutex); //sblocco del mutex
                                        storyPlayers("sta giocando",myPlayer,clientid);
                                    }
                                    else{
                                        if((sendMessage(clientid,"RIPROVA PIU' TARDI, GAME IN CORSO")) == 0)
                                        {
                                            storyPlayers("si è disconnesso",myPlayer,clientid);
                                            startPlayer = deleteListPlayer(startPlayer,myPlayer->idSocket);
                                            close(myPlayer->idSocket);
                                            
                                        }
                                        storyPlayers("non ha trovato game disponibili",myPlayer,clientid);
                         
                                    }
           
                                    //attesa che il gioco finisca
                                    pthread_cond_wait(&condition_endGame,&mutex_endGame);
                                    pthread_mutex_unlock(&mutex_endGame);
                                    
                                    
                                    break;
                                    
                                case 2:
                                    //VISUALIZZA ESTRAZIONE PRECEDENTI
                                    lastNumber = readLastResult();
                                    printf("Eseguo la richiesta del client==> %s",lastNumber);
                                    
                                    if(lastNumber != NULL){
                                        if(sendMessage(myPlayer->idSocket,lastNumber)==0){
                                            storyPlayers("si è disconnesso",myPlayer,clientid);
                                            startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                            close(myPlayer->idSocket);
                                            pthread_exit(NULL);
                                        }
                                        
                                        storyPlayers("ha richiesto l'ultimo numero estratto",myPlayer,clientid);
                                    }
                                    else{
                                        
                                        if(sendMessage(myPlayer->idSocket,"NON CI SONO ANCORA NUMERI ESTRATTI")==0){
                                            storyPlayers("si è disconnesso",myPlayer,clientid);
                                            startPlayer = deleteListPlayer(listPlayer, clientid);
                                            close(myPlayer->idSocket);
                                            pthread_exit(NULL);
                                        }
                                        storyPlayers("ha richiesto l'ultimo numero estratto",myPlayer,clientid);
                                    }
                                    
                                    
                                    
                                    break;
                                    
                                case 3:
                                    //VISUALIZZA UTENTI ONLINE
                                    storyPlayers("ha richiesto la lettura degli utenti online",myPlayer,clientid);
                                    int len=lenList(listPlayer);
                                    
                                    if(sendMessage(myPlayer->idSocket,intToString(len))==0){
                                        
                                        storyPlayers("si è disconnesso",myPlayer,clientid);
                                        startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                        close(myPlayer->idSocket);
                                        pthread_exit(NULL);
                                    }
                                    
                                    
                                    List curr = listPlayer;
                                    
                                    while(curr){
                                        
                                        if((sendMessage(myPlayer->idSocket, curr->elem->username)) == 0)
                                        {
                                            storyPlayers("si è disconnesso",myPlayer,clientid);
                                            startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                            close(myPlayer->idSocket);
                                            pthread_exit(NULL);
                                        }
                                        
                                        
                                        
                                        if(sendMessage(myPlayer->idSocket,intToString(curr->elem->coins)) == 0){
                                            
                                            storyPlayers("si è disconnesso",myPlayer,clientid);
                                            startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                            close(myPlayer->idSocket);
                                            pthread_exit(NULL);
                                        }
                                        
                                        
                                        
                                        curr=curr->next;
                                    }
                                    
                                    break;
                                    
                                case 4:
                                    //INVIO DATI PROFILO DI GIOCO
                                    storyPlayers("ha richiesto la visualizzazione dati account",myPlayer,clientid);
                                    if(sendMessage(myPlayer->idSocket,dati_login) == 0){
                                    
                                        storyPlayers("si è disconnesso",myPlayer,clientid);
                                        startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                                        close(myPlayer->idSocket);
                                        pthread_exit(NULL);
                                    }
                                
                                    break;
                            }
                            
                            
                            
                            
                        } while (sceltaMenuLogInt != 0);
                        
                    }
                    
                    break;
                    
                case 3:
                    //REGOLAMENTO GIOCO
                    storyPlayers("ha richiesto il regolamento",myPlayer,clientid);
                    if(sendMessage(myPlayer->idSocket,rules) == 0){
                        storyPlayers("si è disconnesso",myPlayer,clientid);
                        startPlayer = deleteListPlayer(listPlayer,myPlayer->idSocket);
                        close(myPlayer->idSocket);
                        pthread_exit(NULL);
                    }
                    break;
                    
                
            }
            
            
            
        }  while (scelta != 0);
    }
    else if (strcmp(recv,"SYNC")==0)
    {
        char *user;
        
        //SYNC STEP 2
        if((user = receiveMessage(clientid)) == NULL)
        {
            storyPlayers("si è disconnesso",NULL,clientid);
            close(clientid);
            pthread_exit(NULL);
        }
        
    
        
        Player myPlayer = checkUsername(listPlayer,user);
        pthread_t tidSynch;
        if(myPlayer != NULL)
        {
            myPlayer->socketSync = clientid;
            pthread_create(&tidSynch,NULL,threadSync,myPlayer);
            
        }
        
    }
    pthread_exit(NULL);
    
}
//--------------------------------------------------------------------------------------------------------------------



//THREAD USATO PER IL COUNTDOWN QUANDO PARTE IL GIOCO
void *threadCount(void *arg)
{
    status = "APERTO";
    for(int i = count; i >= 1; i--)
    {
        sleep(1); // dormo un secondo
        printf("\n%d\n",count);
        pthread_mutex_lock(&mutex);
        count--;
        pthread_mutex_unlock(&mutex);
    }
    
    status = "CHIUSO";
    pthread_exit(NULL);
}
//-----------------------------------------------------




//THREAD CHE RICEVE LE SCOMMESSE EFFETTUATE DAI PLAYERS
void *betterCommunication(void *arg){
    
    Player p = (Player)arg;
    char *newBet;
    char *newBid;
    int bet = 0, bid = 0;
    
    
    if (!sendMessage(p->idSocket,"INVIA BETS"))
    {
        
        storyPlayers("si è disconnesso",p,p->idSocket);
        startPlayer = deleteListPlayer(listPlayer,p->idSocket);
        close(p->idSocket);
        pthread_exit(NULL);
    }
    
    
    
    
    if((newBet = receiveMessage(p->idSocket)) == NULL){
        
        storyPlayers("si è disconnesso",p,p->idSocket);
        startPlayer = deleteListPlayer(listPlayer,p->idSocket);
        close(p->idSocket);
        pthread_exit(NULL);
        
    }
    
    if((newBid = receiveMessage(p->idSocket)) == NULL){
        
        storyPlayers("si è disconnesso",p,p->idSocket);
        startPlayer = deleteListPlayer(listPlayer,p->idSocket);
        close(p->idSocket);
        pthread_exit(NULL);
        
    }
  
    bet = atoi(newBet);
    bid = atoi(newBid);
    

    Bid b = NULL;

    p->coins = p->coins - bid;
    b = initBid(bid,bet,p->idSocket,p->username,idGame);
    
    storyPlayers("ha effettuato la giocata",p,p->idSocket);
    
    pthread_mutex_lock(&mutex); //blocco il mutex
    addBidList(&p->listBids,b);//aggiunge la mia giocata in listBids associato alla struttura del player
    pthread_mutex_unlock(&mutex); //sblocco il mutex
    
    
    pthread_cond_wait(&condWaitPlayers,&mutexWaitPlayers);
    pthread_mutex_unlock(&mutexWaitPlayers);
    
    if (!sendMessage(p->idSocket,intToString(res)))
    {
        storyPlayers("si è disconnesso",p,p->idSocket);
        startPlayer = deleteListPlayer(listPlayer,p->idSocket);
        close(p->idSocket);
        pthread_exit(NULL);
    }
    
    
    pthread_exit(NULL);
}

//----------------------------------------------------------------------------------------------------------




//THREAD PRINCIPALE DEL GIOCO (ACCETTA I PLAYERS - FASE SCOMMESSA - ESTRAZIONE NUMERO - FINE GIOCO)
void *startGame(void *arg){
    
    pthread_t tidCount;
    // uint64_t n = 1;
    List curr = NULL;
    
    while(isEmptyList != false)
        
    {
        printf("\nPlayers in attesa...\n");
        pthread_cond_wait(&condition,&mutex);
        pthread_mutex_unlock(&mutex);
        
       
        while(isEmptyList != true){
            
            printf("\nSTART GAME INIZIATO\n");
            //pthread_mutex_unlock(&mutex); //sblocco del mutex
            pthread_create(&tidCount,NULL,threadCount,NULL); //Thread dove parte il countdown di 30sec
            pthread_join(tidCount,NULL); //attende che il threadCount finisca
            
            curr = startPlayer;
            while(curr){
                if(curr->elem->idSocket > 0){
                    pthread_create(&tidBets[indexT],NULL,betterCommunication,curr->elem);
                }
                curr = curr->next;
            }
            
            
            printf("\nScommesse in corso...\n");
            sleep(17);
            res = RouletteGame();
            printf("\nNumero pescato %d\n",res);
            printf("\nGioco Terminato\n");
            write_number(res,idGame);
            pthread_cond_broadcast(&condWaitPlayers);
            pthread_cond_broadcast(&condition);
            status = "APERTO";
            
            sleep(3);
            scorePlayers(startPlayer,res,idGame);
            dealloca(startPlayer); //svuoto la lista
            startPlayer = NULL;
            count = 20;
            idGame++;
            isEmptyList = true;
            
            pthread_cond_broadcast(&condition_endGame);
            
        }
        
        
        
    }

    
    pthread_exit(NULL);
}

//---------------------------------------------------------------------------------------------------

int main(int argc, char const *argv[])
{
    int socksd;
    struct sockaddr_in servAddr, clientAddr;
    
    pthread_t id;
    pthread_t tidGame;
    
    
    
    createDatabase(); //crea la cartella database (se non esiste) con il file players.txt
  
    socksd = connectionConfiguration(&servAddr,true); //connessione della socket
    startConnection(socksd, servAddr);
    
    pthread_create(&tidGame,NULL,startGame,NULL); //crea un thread per startGame
    //pthread_create(&tidCount,NULL,threadCount,NULL);
    
    while(true)
    {
        
        
        int clientfd=acceptConnection(socksd, clientAddr);//viene accettata la connessione del singolo client
        
        pthread_create(&id, NULL, func , &clientfd); //viene creato un Thread func ogni volta accettata la connessione
        
    }
    
    //Chiusura del socket
    close(socksd);
    
    return 0;
}
