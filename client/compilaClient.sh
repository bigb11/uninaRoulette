#!/bin/bash 
 
C_FILES="" 
COMPILER="gcc " 
ARGV="-o client -lpthread && ./client" 
 
 
echo -e "\n\n============== [COMPILE SERVER] ===============" 
echo -e "[=>] COMPILAZIONE DEI SEGUENTI FILE:" 
 
for file in $(find .. -not -path "*server/*" -type f -name "*.c") 
do 
    echo "[.]" $file  
    C_FILES+="${file} " 
done 
 
COMMAND="${COMPILER} ${C_FILES} ${ARGV}" 
echo -e "\n[=>] ${COMMAND}" 
 
eval $COMMAND 
 
exit 0