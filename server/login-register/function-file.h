//
//  function-file.h
//  rouletteGame
//
//  Created by Domenico Maione on 03/05/21.
//

#ifndef function_file_h
#define function_file_h

#include <stdio.h>
#include "../../list/list.h"

#define BUFFER_MAX 128
#define BUFFER_DATI 30
#define BUFFER_COINS 4

typedef struct users
{
    char user[BUFFER_DATI+1]; //Dimensione massima 128  + 1 carattere vuoto
    char nome[BUFFER_DATI+1];
    char cognome[BUFFER_DATI+1];
    char email[BUFFER_DATI+1];
    char passw[BUFFER_DATI+1];
    char coins[BUFFER_COINS+1];
} users;


//int checkRegister(char *username, char *email);
//char  *checkLogin(char *username, char *password);
int check(char *string, char *string2, int scelta, char *dati_login);
char *stringWithFormat(const char *str, const char *startFormat, const char *endFormat);
void initStaticString(char *str, int size);
void logPlayersTxt(List listPlayer);
void storyBoard(BidList topBid);
void storyPlayers(char *testo, Player myPlayer, int clientid);
char *readLastResult();
char *readOnline();


#endif /* function_file_h */
