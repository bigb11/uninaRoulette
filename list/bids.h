//
//  bids.h
//  rouletteGame
//
//  Created by Domenico Maione on 26/05/21.
//

#ifndef bids_h
#define bids_h

#include <stdio.h>

struct TBid
{
    int idGame;
    char *username;
    int socket;
    int bid; //quanto ho puntato
    int bet; //il numero che ho puntato
    int res;
    char *date;
    struct TBid* next;
};

typedef struct TBid* Bid;


Bid initBid(int amount, int bet, int socket, char* username, int idGame);
void addBidList(Bid *list, Bid newBid);
void freeBids(Bid *p);

#endif /* bids_h */
