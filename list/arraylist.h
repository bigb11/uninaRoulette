//
//  arraylist.h
//  rouletteGame
//
//  Created by Domenico Maione on 18/06/21.
//

#ifndef arraylist_h
#define arraylist_h

#include <stdio.h>



struct AList {
    int capacity;
    int size;
    char *name;
    char *surname;
    char *username;
    char *email;
    char *password;
    char *coins;
    int *arr;
};

typedef struct AList* ArrList;


ArrList initialiseWithCapacity(int initialCapacity);
ArrList initialise();


#endif /* arraylist_h */
