//
//  menu.h
//  rouletteGame
//
//  Created by Domenico Maione on 30/05/21.
//

#ifndef menu_h
#define menu_h

#include <stdio.h>
#include "../player/player.h"

int Menu();
int MenuLog(Player myPlayer);
void welcome();
void Pulisci();
char *Register(char *dati);
char *Login(char *dati);

#endif /* menu_h */
