//
//  menu.c
//  rouletteGame
//
//  Created by Domenico Maione on 30/05/21.
//

#include "menu.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../player/player.h"
#include "../string/utilsstring.h"
#include "../server/login-register/function-file.h"

void clearBuffer(){
    char c;
    while ((c = getchar()) != '\n' && c != EOF) { };
}


int getInt(int *data)
{
    int ok, // ritorno della funzione
    i, // indice per scorrere la stringa letta
    negative; // indica se il numero Ã¯Â¿Â½ negativo
    char buffer[51];
    scanf("%50s",buffer);
    clearBuffer();
    i = negative = (buffer[0]=='-') ? 1 : 0 ;
    while( buffer[i]>47 && buffer[i]<58 ) i++;
    ok = (buffer[i]=='\0' && i>negative );
    if(ok) *data = atoi(buffer);
    return ok;
}



void welcome(){
    //MENU
    printf("===============================================================================");
    //printf("\n");
    printf("\n\nBENVENUTO NELLA NOSTRA ROULETTE\n\n") ;
    //printf("\n");
    printf("===============================================================================");
}




int Menu()
{
    int scelta;
    printf("\n\n Puoi effettuare le seguenti operazioni:\n\n [1]REGISTRAZIONE\n [2]LOGIN\n [3]REGOLAMENTO DEL GIOCO\n [0]CHIUDI\n\n --Inserisci la tua scelta [0][1][2][3]: ");
    while(!getInt(&scelta)||scelta<0||scelta>3) {
        printf("\n Scelta errata. -- Inserisci la scelta corretta [0] o [1] o [2] o [3]: ");
    }
    return scelta;
}





int MenuLog(Player myPlayer){
    
    int scelta;
    if(myPlayer->coins==0){
        printf("\n\n Puoi effettuare le seguenti operazioni:\n\n [1]EFFETTUA LA GIOCATA (CREDITO INSUFFICIENTE).\n [2]VISUALIZZA LE ESTRAZIONE PRECEDENTI\n [3]VISUALIZZA UTENTI ONLINE\n [4]DETTAGLI CONTO DI GIOCO\n [0]LOGOUT\n\n --Inserisci la tua scelta [0][1][2][3][4]: ");
        while(!getInt(&scelta)||scelta<0||scelta>4){
            printf("\n Scelta errata. -- Inserisci la scelta corretta [0] o [1] o [2] o [3] o [4]: ");
        }
    }
    
    else{
        
        printf("\n\n Puoi effettuare le seguenti operazioni:\n\n [1]EFFETTUA LA GIOCATA\n [2]VISUALIZZA LE ESTRAZIONE PRECEDENTI\n [3]VISUALIZZA UTENTI ONLINE\n [4]DETTAGLI CONTO DI GIOCO\n [0]LOGOUT\n\n --Inserisci la tua scelta [0][1][2][3][4]: ");
        
        while(!getInt(&scelta)||scelta<0||scelta>4)
        {
            printf("\n Scelta errata. -- Inserisci la scelta corretta [0] o [1] o [2] o [3] o [4]: ");
        }
        
        
    }
    
    return scelta;
}









void Pulisci()
{
   printf("\n\nPremi Enter per continuare... ");
     char prev=0;

        while(1)
        {
            char c = getchar();

            if(c == '\n' && prev == c)
            {

                system("@cls||clear");
                break;
            }

            prev = c;
        }
}


char *Register(char *dati)
{
    
    char *name = (char*) malloc( 100 );
    char *surname = (char*) malloc( 100 );
    char *username = (char*) malloc( 100 );
    char *email = (char*) malloc( 100 );
    char *password = (char*) malloc( 100 );
    char newcoins[BUFFER_COINS];
    char ch='@';
    int coins = 100,lenDati;
    
    printf("\nInserisci nome: ");
    scanf("%s",name);
    
    printf("\nInserisci cognome: ");
    scanf("%s",surname);
    
    printf("\nInserisci username: ");
    scanf("%s",username);
    
    printf("\nInserisci email senza provider (@unina.it): ");
    scanf("%s",email);
    while(strchr(email, ch)!=NULL)
    {
        printf("\n =>Email errata. --- Inserire solo nome utente(senza provider): ");
        scanf("%s",email);
    }
    strcat(email,"@unina.it");
    
    printf("\nInserisci password: ");
    scanf("%s",password);
    
    
    sprintf(newcoins,"%d",coins); //Trasformo un numero intero in carattere
    
    lenDati= dimDati(username,name,surname,email,password,newcoins) + 30;
    
    dati = calloc(lenDati,sizeof(char));
    snprintf(dati,lenDati,"U:[%s] N:[%s] S:[%s] E:[%s] P:[%s] C:[%s]",username,name,surname,email,password,newcoins);
    
    
    free(name);
    free(surname);
    free(username);
    free(email);
    free(password);
    
    return dati;
    
}

char *Login(char *dati)
{
    char *username = (char*) malloc( 100 );
    char *password = (char*) malloc( 100 );
    int lenDati;
    
    printf("\nInserisci username:");
    scanf("%s",username);
    
    printf("\nInserisci password:");
    scanf("%s",password);
    
    
    lenDati = dimDatiLogin(username,password) + 50; 
    //printf("\nLENDATI: %d\n",lenDati);
    dati = calloc(lenDati,sizeof(char));
    snprintf(dati,lenDati,"U:[%s] N:[%s] S:[%s] E:[%s] P:[%s] C:[%s]",username,"null","null","null",password,"null");
    
    free(username);
    free(password);
    
    return dati;
}
